package gestiondestock.metier;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gestiondestock.beans.Item;
import gestiondestock.beans.Order;
import gestiondestock.persistence.DBUtils;

public class StockManagement {
	
	public static DBUtils querier;
	
	public static boolean connectToDabaseFromLogin(String dbType, String hostName, String dbName, String userName, String password) {
		querier = new DBUtils(dbType,hostName, dbName, userName,password);
		return (DBUtils.conn != null);
	}
	
	public static String getNewOrderCode() throws SQLException {
		int idx = querier.getLastOrderIndex();
		String code = String.format("%0" + (6-String.valueOf(idx).length()) + "d", idx);
		code += "O";
		
		return code;
	}
	
	public static void finalizeOrder(Order order) throws SQLException {
		querier.insertOrder(order);
		for (Item item : order.getItems())
			querier.updateQuantity(item.getCode(), item.getStock_quantity()-1);
	}
	
	public static void validateRegisterClosing(List<Order> orders) throws SQLException {
		for (Order order : orders)
			querier.updateOrder(order.getCode(), true);
	}
	
	public static boolean existsInCatalog(String code) throws SQLException {
		return (querier.findItem(code) != null);
	}
	
	public static void addItem(Item item) throws SQLException {
		if (!existsInCatalog(item.getCode()))
			querier.insertItem(item);
	}
	
	public static void removeItem(String code) throws SQLException {
		if (existsInCatalog(code))
			querier.deleteItem(code);
	}
	
	public static List<Order> getAllOrders(Date today) throws SQLException {
		List<Order> orders = new ArrayList<Order>();

		Order order;
		
		List<String> codes = querier.listOpenOrders(today, false);
		for (String code : codes) {
			order = querier.findOrder(code);
			orders.add(order);
		}		
		return orders;
	}

}
 