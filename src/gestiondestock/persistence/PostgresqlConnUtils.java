package gestiondestock.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLInvalidAuthorizationSpecException;
public class PostgresqlConnUtils {

	public static Connection getPostgresqlDBConnection(String hostName, String dbName,
			String userName, String password) throws SQLException,
	ClassNotFoundException, SQLInvalidAuthorizationSpecException {

		Class.forName("org.postgresql.Driver");
		String connectionURL = "jdbc:postgresql://" + hostName + ":5432/" + dbName;

		Connection conn = DriverManager.getConnection(connectionURL, userName,
				password);
		return conn;
	}
}