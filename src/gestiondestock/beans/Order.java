package gestiondestock.beans;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Order {

	private String code;
	private Date date;
	private List<Item> items;
	private double total = 0.0;
	
	public Order(String code, Date date) {
		this.code = code;
		this.date = date;
	}
	
	public Order(String code, Date date, List<Item> items) {
		this.code = code;
		this.date = date;
		this.items = items;
		this.computeTotal();
	}
	
	public void addItem(Item item) {
		if (items == null)
			this.items = new ArrayList<Item>();
		items.add(item);
		this.total += item.getPrice();
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Item> getItems() {
		return items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
		this.computeTotal();
	}
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	private void computeTotal() {
		double total = 0.0;
		
		for (Item it : this.items)
			total += it.getPrice();
		
		this.total = total;
	}
	
	
}