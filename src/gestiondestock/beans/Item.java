package gestiondestock.beans;

public class Item {

	private String code;
	private String name;
	private float price;
	private Provider provider;
	private Integer stock_range;
	private Integer stock_quantity;
	private boolean sold_by_unit;
	
	public Item(String code) {
		this.code = code;
	}	
	
	public Item(String code, String name, float price) {
		super();
		this.code = code;
		this.name = name;
		this.price = price;
	}

	public Item(String code, String name, float price, Provider provider, Integer stock_range, Integer stock_quantity,
			boolean sold_by_unit) {
		this.code = code;
		this.name = name;
		this.price = price;
		this.provider = provider;
		this.stock_range = stock_range;
		this.stock_quantity = stock_quantity;
		this.sold_by_unit = sold_by_unit;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Provider getProvider() {
		return provider;
	}

	public void setProvider(Provider provider) {
		this.provider = provider;
	}

	public Integer getStock_range() {
		return stock_range;
	}

	public void setStock_range(Integer stock_range) {
		this.stock_range = stock_range;
	}

	public Integer getStock_quantity() {
		return stock_quantity;
	}

	public void setStock_quantity(Integer stock_quantity) {
		this.stock_quantity = stock_quantity;
	}

	public boolean isSold_by_unit() {
		return sold_by_unit;
	}

	public void setSold_by_unit(boolean sold_by_unit) {
		this.sold_by_unit = sold_by_unit;
	}

}