package gestiondestock.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JSeparator;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import gestiondestock.beans.Item;
import gestiondestock.beans.Order;
import gestiondestock.metier.StockManagement;


public class MenuWindow {

	public JFrame frmGestionDeStock;
	private JTextField textField;
	private JLabel lblOrder;
	private JLabel lblStatus;
	private JLabel lblTotal;
	private DefaultTableModel tableModel;
	private String[] columns = {"Code", "Nom", "Prix"};
	private JTable table;
	
	private Order order = null;
	private String orderCode;
	private Date date = new java.sql.Date(Calendar.getInstance().getTimeInMillis());

	/**
	 * Create the application.
	 */
	public MenuWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGestionDeStock = new JFrame();
		frmGestionDeStock.setTitle("Gestion de Stock");
		frmGestionDeStock.setBounds(100, 100, 771, 474);
		frmGestionDeStock.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGestionDeStock.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JLabel lblGestionDeStock = new JLabel("Gestion de Stock");
		lblGestionDeStock.setHorizontalAlignment(SwingConstants.CENTER);
		frmGestionDeStock.getContentPane().add(lblGestionDeStock, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		frmGestionDeStock.getContentPane().add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JButton btnPayer = new JButton("Payer");
		btnPayer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (order == null || order.getItems() == null || order.getItems().size() == 0)
					return;
				try {
					StockManagement.finalizeOrder(order);
					PaymentDetailsWindow paymentWindow = new PaymentDetailsWindow(order);
					paymentWindow.setVisible(true);
					resetForm();
				} catch (SQLException e) {
					showError(e.getMessage());
				}
			}
		});
		
		JLabel lblDate = new JLabel("Date: " + date.toString());
		panel.add(lblDate);
		
		lblOrder = new JLabel("No. Commande:");
		panel.add(lblOrder);
		
		lblTotal = new JLabel("Total: 0.0");
		panel.add(lblTotal);
		panel.add(btnPayer);
		
		JButton btnEtablissementDeSolde = new JButton("Etablissement de solde");
		btnEtablissementDeSolde.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RegisterClosingWindow regClosing = new RegisterClosingWindow(frmGestionDeStock);
				regClosing.setVisible(true);
			}
		});
		
		JSeparator separator = new JSeparator();
		panel.add(separator);
		panel.add(btnEtablissementDeSolde);
		
		JPanel panel_1 = new JPanel();
		frmGestionDeStock.getContentPane().add(panel_1, BorderLayout.SOUTH);
		
		JButton btnScanner = new JButton("Scanner");
		btnScanner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code= getItemCode();
				
				if (!code.trim().isEmpty()) {
					addItemToTable(code);
					textField.setText("");
				}
			}
		});
		panel_1.setLayout(new BorderLayout(0, 0));
		panel_1.add(btnScanner, BorderLayout.CENTER);
		
		lblStatus = new JLabel("Aucun message.");
		panel_1.add(lblStatus, BorderLayout.SOUTH);
		
		textField = new JTextField();
		panel_1.add(textField, BorderLayout.WEST);
		textField.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		frmGestionDeStock.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		tableModel = (DefaultTableModel) table.getModel();
		tableModel.setColumnIdentifiers(columns);
		table.setModel(tableModel);
		
		/* DB queries */
		updateOrderCode();
	}
	
	protected String getItemCode() {
		String itemCode = textField.getText();
		// TODO Sanitize input
		return itemCode;
	}
	
	protected void updateOrderCode() {
		try {
			orderCode = StockManagement.getNewOrderCode();
			lblOrder.setText("Commande No." + orderCode);
		} catch (SQLException e) {
			showError(e.getMessage());		
		}		
	}
	
	protected void showError (String message) {
		lblStatus.setText(message);
	}
	
	protected void addItemToOrder(Item item) {
		if (order == null)
		{
			List<Item> items = new ArrayList<Item>();
			order = new Order(orderCode, date, items);
		}
		order.addItem(item);
	}	
	
	protected void addItemToTable(String code) {
		try {
			Item item = StockManagement.querier.findItem(code);
			if (item == null) {
				showError("Article " + code + " non trouvé.");
				return;
			}
			Object[] rowData = {item.getCode(), item.getName(), item.getPrice()};
			
			tableModel.addRow(rowData);			
			addItemToOrder(item);
			lblTotal.setText(String.format("Total: %.2f", order.getTotal()));
		} catch (SQLException e) {
			showError(e.getMessage());	
		}
	}
	
	protected void resetForm() {
		order = null;
		updateOrderCode();
		int rowCount = tableModel.getRowCount();
		for (int i = rowCount - 1; i >= 0; i--) {
			tableModel.removeRow(i);
		}
		lblTotal.setText("Total: 0.0");
	}
}
