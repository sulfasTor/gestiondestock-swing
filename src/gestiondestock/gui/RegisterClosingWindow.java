package gestiondestock.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JSplitPane;

import gestiondestock.beans.Item;
import gestiondestock.beans.Order;
import gestiondestock.metier.StockManagement;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class RegisterClosingWindow extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private JTable table_1;

	/**
	 * Create the dialog.
	 * 
	 * @param frmGestionDeStock
	 */
	public RegisterClosingWindow(JFrame frame) {
		super(frame, "Etablissement de solde", true);
		java.util.Date currentDate = Calendar.getInstance().getTime();
		Date today = new Date(currentDate.getTime());
		List<Order> orders;
		try {
			orders = StockManagement.getAllOrders(today);
		} catch (SQLException e) {
			return;
		}

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 376, 455);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JLabel lblEtablissementDeSolde = new JLabel("Etablissement de solde du " + today.toString());
			contentPanel.add(lblEtablissementDeSolde, BorderLayout.NORTH);
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			panel.setLayout(new BorderLayout(0, 0));
			{
				float total = 0;
				for (Order order : orders)
					total += order.getTotal();
				JLabel lblTotal = new JLabel("Total: " + String.format("%.2f", total));
				panel.add(lblTotal, BorderLayout.EAST);

			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			splitPane.setResizeWeight(0.5);
			splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			contentPanel.add(splitPane, BorderLayout.CENTER);
			{
				JScrollPane scrollPane = new JScrollPane();
				splitPane.setRightComponent(scrollPane);
				{
					table = new JTable();
					scrollPane.setViewportView(table);
				}
			}
			{
				JScrollPane scrollPane = new JScrollPane();
				splitPane.setLeftComponent(scrollPane);
				{
					table_1 = new JTable();
					scrollPane.setViewportView(table_1);
				}
			}
		}
		fillTables(orders);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton btnValider = new JButton("Valider");
				btnValider.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (orders != null && orders.size() == 0)
							return;
						JOptionPane.showMessageDialog(null, "Les détails des commandes seront validés.", "TITLE",
								JOptionPane.WARNING_MESSAGE);
						try {
							StockManagement.validateRegisterClosing(orders);
							orders.clear();
							clearTables();
						} catch (SQLException e1) {
						}
					}
				});
				buttonPane.add(btnValider);
			}
		}
	}

	protected void fillTables(List<Order> orders) {
		DefaultTableModel mt1 = (DefaultTableModel) table.getModel();
		DefaultTableModel mt2 = (DefaultTableModel) table_1.getModel();
		String[] columns1 = { "No. Commande", "Produit", "Prix" };
		String[] columns2 = { "No. Commande", "Nb. Articles", "Total" };

		mt1.setColumnIdentifiers(columns1);
		mt2.setColumnIdentifiers(columns2);

		for (Order order : orders) {
			for (Item item : order.getItems()) {
				Object[] rowData = { order.getCode(), item.getName(), item.getPrice() };
				mt1.addRow(rowData);
			}
			Object[] rowData = { order.getCode(), order.getItems().size(), String.format("%.2f", order.getTotal()) };
			mt2.addRow(rowData);
		}

		table.setModel(mt1);
		table_1.setModel(mt2);
	}

	protected void clearTables() {
		DefaultTableModel mt1 = (DefaultTableModel) table.getModel();
		DefaultTableModel mt2 = (DefaultTableModel) table_1.getModel();
		mt1.setRowCount(0);
		mt2.setRowCount(0);
	}
}
