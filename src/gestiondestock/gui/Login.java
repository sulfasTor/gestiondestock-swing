package gestiondestock.gui;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.EventQueue;
import javax.swing.SwingConstants;

import gestiondestock.metier.StockManagement;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class Login {

	private JFrame frmConnectionBaseDe;
	private JTextField txtLocalhost;
	private JTextField txtGestion;
	private JPasswordField passwordField;
	private JTextField txtRoot;
	private JLabel lblStatus;
	private JComboBox<String> comboBox;
	/* Fillers */
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JLabel label_3;
	private JLabel label_4;
	private JLabel label_5;
	private JLabel label_6;
	private JLabel label_7;
	private JLabel label_8;
	private JLabel label_9;
	private JLabel label_10;
	private JLabel label_11;
	private JLabel label_12;
	private JLabel label_13;
	private JLabel label_14;
	private JLabel label_15;
	private JLabel label_16;
	private JLabel label_17;
	private JLabel label_18;
	private JLabel label_19;
	private JLabel label_20;
	private JLabel label_21;
	private JLabel label_22;
	private JLabel label_23;
	private JLabel label_24;
	private JLabel label_25;
	private JLabel label_26;
	private JLabel label_27;
	private JLabel label_28;
	private JLabel label_29;
	private JLabel label_30;
	private JLabel label_31;
	private JLabel label_32;
	private JLabel label_33;
	private JLabel label_34;
	private JLabel label_35;
	private JLabel label_36;
	private JLabel label_37;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmConnectionBaseDe.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmConnectionBaseDe = new JFrame();
		frmConnectionBaseDe.setTitle("Connection Base de donnée");
		frmConnectionBaseDe.setBounds(500, 100, 496, 312);
		frmConnectionBaseDe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmConnectionBaseDe.getContentPane().setLayout(new GridLayout(0, 5, 0, 0));
		
		label = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label);
		
		label_1 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_1);
		
		label_2 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_2);
		
		label_3 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_3);
		
		label_4 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_4);
		
		label_5 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_5);
		
		JLabel lblTypeBd = new JLabel("Type BD");
		frmConnectionBaseDe.getContentPane().add(lblTypeBd);
		
		label_6 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_6);
		
		comboBox = new JComboBox<String>();
		comboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"mariadb", "postgresql"}));
		frmConnectionBaseDe.getContentPane().add(comboBox);
		
		label_7 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_7);
		
		label_8 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_8);
		
		JLabel lblAddressBd = new JLabel("Addresse BD");
		frmConnectionBaseDe.getContentPane().add(lblAddressBd);
		
		label_9 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_9);
		
		txtLocalhost = new JTextField();
		txtLocalhost.setText("localhost");
		frmConnectionBaseDe.getContentPane().add(txtLocalhost);
		txtLocalhost.setColumns(10);
		
		label_10 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_10);
		
		label_11 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_11);
		
		JLabel lblNomBd = new JLabel("Nom BD");
		frmConnectionBaseDe.getContentPane().add(lblNomBd);
		
		label_12 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_12);
		
		txtGestion = new JTextField();
		txtGestion.setText("GESTION");
		frmConnectionBaseDe.getContentPane().add(txtGestion);
		txtGestion.setColumns(10);
		
		label_13 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_13);
		
		label_14 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_14);
		
		JLabel lblUtilisateur = new JLabel("Utilisateur");
		lblUtilisateur.setHorizontalAlignment(SwingConstants.LEFT);
		frmConnectionBaseDe.getContentPane().add(lblUtilisateur);
		
		label_15 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_15);
		
		txtRoot = new JTextField();
		txtRoot.setText("root");
		frmConnectionBaseDe.getContentPane().add(txtRoot);
		txtRoot.setColumns(10);
		
		label_16 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_16);
		
		label_17 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_17);
		
		JLabel lblPassword = new JLabel("Mot de passe");
		lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		frmConnectionBaseDe.getContentPane().add(lblPassword);
		
		label_18 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_18);
		
		passwordField = new JPasswordField();
		frmConnectionBaseDe.getContentPane().add(passwordField);
		
		label_19 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_19);
		
		label_20 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_20);
		
		label_21 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_21);
		
		label_22 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_22);
		
		label_23 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_23);
		
		label_24 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_24);
		
		label_25 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_25);
		
		label_26 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_26);
		
		label_27 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_27);
		
		label_28 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_28);
		
		label_29 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_29);
		
		label_30 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_30);
		
		label_31 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_31);
		
		label_32 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_32);
		
		label_33 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_33);
		
		label_34 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_34);
		
		label_35 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_35);
		
		lblStatus = new JLabel("Non connecté.");
		frmConnectionBaseDe.getContentPane().add(lblStatus);
		
		label_36 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_36);
		
		JButton btnConnecter = new JButton("Connecter");
		btnConnecter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String dbType = (String) comboBox.getSelectedItem();
				String hostName = txtLocalhost.getText();
				String dbName = txtGestion.getText();
				String user = txtRoot.getText();
				String password = new String(passwordField.getPassword());
				
				if (StockManagement.connectToDabaseFromLogin(dbType, hostName, dbName, user, password)) {
					lblStatus.setText("Connecté.");
					MenuWindow menu = new MenuWindow();
					menu.frmGestionDeStock.setVisible(true);
					frmConnectionBaseDe.dispose();
				} else {
					lblStatus.setText("Echouée.");					
				}
			}
		});
		frmConnectionBaseDe.getContentPane().add(btnConnecter);
		
		label_37 = new JLabel("");
		frmConnectionBaseDe.getContentPane().add(label_37);
	}
}
