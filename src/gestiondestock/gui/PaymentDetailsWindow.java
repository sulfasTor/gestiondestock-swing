package gestiondestock.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JLabel;

import gestiondestock.beans.Item;
import gestiondestock.beans.Order;

@SuppressWarnings("serial")
public class PaymentDetailsWindow extends JDialog {

	/**
	 * 
	 */
	private final JPanel contentPanel = new JPanel();
	private DefaultTableModel modelTable;
	private JTable table;

	/**
	 * Create the dialog.
	 */
	public PaymentDetailsWindow(Order order) {
		setTitle("Détails de la commande " + order.getDate().toString());
		setBounds(200, 200, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JScrollPane scrollPane = new JScrollPane();
			contentPanel.add(scrollPane);
			{
				table = new JTable();
				table.setRowSelectionAllowed(false);
				modelTable = (DefaultTableModel) table.getModel();
				table.setModel(modelTable);
				scrollPane.setViewportView(table);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel, BorderLayout.SOUTH);
			panel.setLayout(new BorderLayout(0, 0));
			{
				JLabel lblNoCommande = new JLabel("No. Commande: " + order.getCode());
				panel.add(lblNoCommande);
			}
			{
				JLabel lblTotal = new JLabel("Total:" + String.format("%.2f", order.getTotal()));
				panel.add(lblTotal, BorderLayout.EAST);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		
		fillTable(order);
	}
	
	protected void fillTable(Order order) {
		String[] columns = {"Code", "Nom", "Prix"};
		modelTable.setColumnIdentifiers(columns);
		
		for (Item item : order.getItems()) {
			Object[] rowData = {item.getCode(), item.getName(), item.getPrice()};
			modelTable.addRow(rowData);
		}
	}
}
